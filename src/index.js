import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from '@reach/router'
import {Helmet} from 'react-helmet';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Home from './pages/Home'
import ForgotPasswordCheckPage from './pages/ForgotPasswordPage'

/*
const rootElement = document.getElementById('root')
ReactDOM.render(
  <Router>
    <App default />
  </Router>,
  rootElement
)
*/



ReactDOM.render(
  <React.StrictMode>+
      <Router>
    <App default />
    <Home default />
    <ForgotPasswordCheckPage path="/forgotpasswordpage" />
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
