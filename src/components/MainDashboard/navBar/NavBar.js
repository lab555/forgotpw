import React from 'react';
import { Router } from '@reach/router';
import './NavBar.css';



function NavBar() {
  return (

    <main>
      <nav>
        <ul>
          <li><a href="/">Home</a></li>
          <li><a href="/forgotpasswordpage">Forgot Password Page</a></li>
        </ul>
      </nav>
    </main>

  );
 }
 export default NavBar;




