
import React, { useReducer, useState } from 'react';
//import '../../components/SignIn/SignIn.scss';
import './ForgotPassword.css';

const formReducer = (state, event) => {
    return {
        ...state,
        [event.name]: event.value
    }
}

function ResetPwForm() {
    //const [siteid, setSiteId] = '22';
    const [formData, setFormData] = useReducer(formReducer, {});
    const [submitting, setSubmitting] = useState(false);



    const handleSubmit = event => {
        event.preventDefault();


        //form data into body of request as JSON string
/*
        const url = 'https://jsonplaceholder.typicode.com/posts'
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({username})
        };
        fetch(url, requestOptions)
            .then(response => console.log('Submitted successfully'))
            .catch(error => console.log('Form submit error', error))
    };
*/


/*
        //testing state
        alert(`Your state values: \n 
            You can replace this alert with your process`);
        // end testing state
        */




        setSubmitting(true);

        setTimeout(() => {
            setSubmitting(false);
        }, 3000);
    }




    const handleChange = event => {
        setFormData({
            name: event.target.name,
            value: event.target.value,
        });
    }
    return(
        <div className="resetPwForm">
            <p>Please enter your details. Your information will be sent to your email address on file.</p>
            <form onSubmit={handleSubmit}>
                <fieldset disabled={submitting}>
                    <label>
                        <p>Username</p>
                        <input
                            value={formData.username || ''}
                            onChange={handleChange}
                            placeholder="Username"
                            type="email"
                            name="username"
                            required
                        />
                    </label>
                </fieldset>
                <button type="submit" disabled={submitting}>Submit</button>
            </form>
        </div>
    )
}

export default ResetPwForm;



/*
import React, { useState } from "react";
import '../../components/SignIn/SignIn.scss';
import './ForgotPassword.scss';
function ResetPwForm() {
    const [email, setEmail] = useState("");
    const [siteid, setSiteId] = '22';
    return (
        <form>
            <p>Please enter your details and an email will be sent to your email address on file:</p>
            <input
                value={email}
                onChange={e => setEmail(e.target.value)}
                placeholder="Email address"
                type="email"
                name="email"
                required
            />

            <button type="submit">Submit</button>
        </form>
    );
}
export default ResetPwForm;


 */
