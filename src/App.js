import React, {Suspense}  from 'react';
import {Helmet} from "react-helmet";
import { Router } from '@reach/router';
import LoadingMask from "./components/LoadingMask/LoadingMask";
import MainDashboard from './pages/MainDashBoard/MainDashboard';
import IframeForgotPassword from './components/iframeForgotPassword/'
import ForgotPassword from './components/MainDashboard/forgotPassword/ForgotPassword.js'
import NavBar from './components/MainDashboard/navBar/NavBar.js'

import logo from './logo.svg';
import './App.css';



function App() {
  return (

    <div className="App">
        <Helmet>
            <meta name="description" content={process.env.REACT_APP_SITE_DESCRIPTION}/>
        </Helmet>
        <Suspense fallback={<div className="SuspenseLoader">Loading...</div>}>
            {isIE ? <OutDatedBrowserComponent/> : <MainDashboard/>}
        </Suspense>

            <NavBar />

      <header className="App-header">


        <img src={logo} className="App-logo" alt="logo" />
        <p>
         Reset Password - Test Site
        </p>
        <p><IframeForgotPassword /></p>
        <p>Added: react-helmet |  @reach/router</p>
        <p>
        <a className="App-link" href="https://beta.trentonducati.com" target="_blank"  rel="noopener noreferrer">   beta.trentonducati.com        </a>
        </p>
        <p><ForgotPassword /></p>
      </header>

    </div>
  );
}

export default App;
