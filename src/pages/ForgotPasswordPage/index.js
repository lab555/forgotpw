import * as React from 'react'
import ForgotPassword from '../../components/forgotPassword/ForgotPassword.js'
import IframeForgotPassword from '../../components/iframeForgotPassword/'

function ForgotPasswordPage() {
  return (
    <div>
      <h4>Forgot Password Form</h4>
      <ForgotPassword />
       <h4>Forgot Password Iframe</h4>
      <IframeForgotPassword />
    </div>
  )
}

export default ForgotPasswordPage