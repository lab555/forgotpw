import * as React from 'react'
import ForgotPassword from '../../components/forgotPassword/'
import IframeForgotPassword from '../../components/iframeForgotPassword/'

function ForgotPasswordCheckPage() {
  return (
    <div>
      <h4>Forgot Password Form</h4>
      <ForgotPassword />
       <h4>Forgot Password Iframe</h4>
      <IframeForgotPassword />
    </div>
  )
}

export default ForgotPasswordCheckPage